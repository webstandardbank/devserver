package za.co.sb.devserver.rest.vo;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author steven
 */
public class DeleteResult {
    
    @Schema(example = "MyService", description = "Name of the server for which the delete request was received", name = "serverName")
    String serverName;
    
    @Schema(name = "errorDesc", example = "No such server found to delete", description = "Text describing any error that occurred")
    String errorDesc;

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }
    
    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }
    
    
}
