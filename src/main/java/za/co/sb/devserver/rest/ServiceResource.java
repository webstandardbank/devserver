package za.co.sb.devserver.rest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import za.co.sb.devserver.entity.Register;
import za.co.sb.devserver.rest.vo.RegisterListVO;
import za.co.sb.devserver.rest.vo.RegisterResultVO;
import za.co.sb.devserver.rest.vo.ServiceVO;
import za.co.sb.devserver.rest.vo.DeleteResult;
import za.co.sb.devserver.services.RegisterService;
import za.co.sb.devserver.services.ValidationException;

/**
 *
 * @author steven
 */
@ApplicationScoped
@Path("/services")
public class ServiceResource {

    @Inject
    RegisterService registerService;

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "Register your server endpoints so that it is notified of Andon Cord Status changes.",
            description = "If registration is successful, your server will receive Andon Cord Status changes and an ordered list of other servers when they change."
            + " Your server needs to invoke the server in the list directly after your server (identified by name) in the list.")
    @APIResponse(responseCode = "202",
            description = "Results of the registration request. Registered property would be true. Your server should receive a list of registered servers call.",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = RegisterResultVO.class)))
    @APIResponse(responseCode = "400",
            description = "Registration request had errors. The registered property would be false and an error array of errors would be supplied",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = RegisterResultVO.class)))
    @APIResponse(responseCode = "500",
            description = "Server Error occurred. No body supplied",
            content = @Content(
                    mediaType = "application/json"))
    @RequestBody(name = "RegisterVO",
            content = @Content(
                    mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = ServiceVO.class)))
    @Counted(unit = MetricUnits.NONE,
            name = "serverRegisterCounter",
            absolute = true,
            monotonic = true,
            displayName = "Server Registration Counter",
            description = "Metrics to show how many times servers have called the registration method.")
    public Response register(@Valid ServiceVO register) {
        try {            
            Register reg = buildRegisterEntity(register);                        
            registerService.createOrUpdateRegister(reg);
            return Response.accepted(new RegisterResultVO(true)).build();
        } catch (ValidationException ex) {            
            RegisterResultVO regRes = new RegisterResultVO(false);
            String[] errors = new String[ex.getViolations().size()];
            errors = ex.getViolations().toArray(errors);
            regRes.setErrors(errors);
            return Response.status(Response.Status.BAD_REQUEST).entity(regRes).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "Get a list of the registered servers",
            description = "Returns a list of all the servers that have been registered with the DevService.")
    @APIResponse(responseCode = "200",
            description = "List of registered servers.",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = ServiceVO.class)))
    @Counted(unit = MetricUnits.NONE,
            name = "serverRegisterListCounter",
            absolute = true,
            monotonic = true,
            displayName = "Server Registration List Counter",
            description = "Metrics to show how many times servers have called the registration method to receive a list of registered servers.")
    public Response register() {
        try {
            RegisterListVO regList = new RegisterListVO();
            regList.setServersList(registerService.getListOfServers());
            return Response.ok(regList).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "Delete a registered server by name",
            description = "Delete a registered server. For example, use this if the server is not responding to your Andon Cord call")
    @APIResponse(responseCode = "200",
            description = "Results of the delete registration request.",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = DeleteResult.class)))
    @APIResponse(responseCode = "404",
            description = "Delete didn't work as the server to delete was not found. Error description will be in errorDesc field.",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = DeleteResult.class)))
    @Counted(unit = MetricUnits.NONE,
            name = "serverDeregisterCounter",
            absolute = true,
            monotonic = true,
            displayName = "Server Deregistration counter",
            description = "Metrics to show how many times servers have called the deregister method.")
    public Response delete(@QueryParam("serviceName") String serviceName, @QueryParam("force") boolean force) {        
        DeleteResult delRes = registerService.delete(serviceName, force);        
        if (delRes.getErrorDesc() == null || delRes.getErrorDesc().isEmpty()) {
            return Response.ok().entity(delRes).build();
        }
        return Response.status(404).entity(delRes).build();
    }

    private Register buildRegisterEntity(ServiceVO registerVO) throws ValidationException {
        try {
            Register reg = new Register();
            reg.setAndonCordStateChangeUrl(new URL(registerVO.getAndonCordStateChangeUrl()));
            reg.setEmail(registerVO.getEmail());
            reg.setLocation(registerVO.getLocation());
            reg.setMetricsUrl(new URL(registerVO.getMetricsUrl()));
            reg.setRegistrationUrl(new URL(registerVO.getRegistrationUrl()));
            reg.setServiceName(registerVO.getServiceName());
            reg.setHealthCheckUrl(new URL(registerVO.getHealthCheckUrl()));
            return reg;
        } catch (MalformedURLException mUrlEx) {
            Set<String> errors = new HashSet();
            errors.add(mUrlEx.getMessage());
            throw new ValidationException(errors);
        }
    }

}
